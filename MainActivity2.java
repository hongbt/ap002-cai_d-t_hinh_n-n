package com.example.ap03;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.io.IOException;

public class MainActivity2 extends AppCompatActivity {

    ImageView IMG_HienThi2;
    Button Btn_ĐLHN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main2);

        // Khởi tạo các thành phần giao diện
        IMG_HienThi2 = findViewById(R.id.IMG_HienThi2);
        Btn_ĐLHN = findViewById(R.id.Btn_ĐLHN);

        // Lấy đường dẫn của hình ảnh từ Intent
        String imageUriString = getIntent().getStringExtra("imageUri");
        if (imageUriString != null) {
            Uri imageUri = Uri.parse(imageUriString);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                // Hiển thị hình ảnh trong ImageView
                IMG_HienThi2.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Btn_ĐLHN.setOnClickListener(v -> {
            // Đặt hình ảnh được chọn làm hình nền
            if (imageUriString != null) {
                try {
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
                    wallpaperManager.setBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(imageUriString)));

                    // Tạo Intent để trở lại MainActivity
                    Intent intent = new Intent(MainActivity2.this, MainActivity.class);
                    // Khởi chạy Intent để trở lại MainActivity
                    startActivity(intent);
                    // Kết thúc MainActivity2
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
