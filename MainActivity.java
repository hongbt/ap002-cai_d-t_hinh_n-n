package com.example.ap03;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    ImageView IMG_HienThi;
    Button Btn_luachon, Btn_DungLam, Btn_Exit;

    // Khai báo biến selectedImageUri ở mức lớp
    private Uri selectedImageUri;

    private ActivityResultLauncher<Intent> imagePickerLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Khởi tạo các thành phần giao diện
        IMG_HienThi = findViewById(R.id.IMG_HienThi);
        Btn_luachon = findViewById(R.id.Btn_luachon);
        Btn_DungLam = findViewById(R.id.Btn_DungLam);
        Btn_Exit = findViewById(R.id.Btn_Exit);

        // Khởi tạo ActivityResultLauncher để xử lý kết quả của Intent
        imagePickerLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                        // Nếu kết quả trả về thành công và dữ liệu không rỗng, tiến hành xử lý ảnh được chọn
                        selectedImageUri = result.getData().getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                            // Hiển thị ảnh được chọn trong ImageView
                            IMG_HienThi.setImageBitmap(bitmap);
                        } catch (Exception e) {
                            // Bắt và in ra ngoại lệ nếu có lỗi xử lý ảnh
                            e.printStackTrace();
                        }
                    }
                });

        Btn_DungLam.setOnClickListener(v -> {
            // Kiểm tra nếu không có ảnh được chọn thì không chuyển sang MainActivity2
            if (selectedImageUri != null) {
                // Tạo Intent để chuyển hướng từ MainActivity sang MainActivity2
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                // Truyền đường dẫn của hình ảnh được chọn qua Intent
                intent.putExtra("imageUri", selectedImageUri.toString());
                // Khởi chạy Intent để chuyển hướng
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this, "Vui lòng chọn một hình ảnh trước khi chuyển đến MainActivity2", Toast.LENGTH_SHORT).show();
            }
        });

        // Thiết lập sự kiện click cho nút Btn_luachon để chọn ảnh
        Btn_luachon.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Khởi chạy Intent để chọn ảnh và sử dụng ActivityResultLauncher để xử lý kết quả trả về
            imagePickerLauncher.launch(intent);
        });

        //thiết lập sự kiện thoát cho Btn_Exit
        Btn_Exit.setOnClickListener(v -> {
            // Kết thúc hoạt động hiện tại (MainActivity)
            finish();
            // Kết thúc tất cả các hoạt động và thoát khỏi ứng dụng
            finishAffinity();
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets( WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
